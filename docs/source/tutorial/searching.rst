.. include:: ../global.rst.inc

Searching for Alerts
====================

ANTARES has indexed its alert database using ElasticSearch so that
the community can perform quick searches.

The ``search`` module of the ANTARES client provides functionality for
searching the ANTARES ElasticSearch database. 

Writing ElasticSearch queries is beyond the scope of this documentation but
visit the instructions on the ANTARES `Advanced Search`_ page for a short introduction.

Let's say that we are interested in finding all of the alerts with:

    * A right ascension of approximately 110 degrees (+- 0.1 degree)
    * A ZTF real/bogus score greater than 0.9

We represent this query in Python as follows:

.. code:: python

   query = {
       "query": {
           "bool": {
               "must": [
                   {
                       "range": {
                           "ra": {
                               "gte": 109.9,
                               "lte": 110.1,
                           } 
                       }
                   },
                   {
                       "range": {
                           "properties.ztf_rb": {
                               "gte": 0.9,
                           } 
                       }
                   }
                ]
           }
       }
   }

And can search through the ANTARES database for matching alerts:

.. code:: python

   from antares_client.search import search
   result_set = search(query) 

We can also download the result set as a compressed/uncompressed (gzipped)
``.json`` or ``.csv`` file:

.. code:: python

   from antares_client.search import download
   result_set = download(query, "results.csv", format="csv", decompress=True) 
   result_set = download(query, "results.csv.gz", format="csv", decompress=False) 
   result_set = download(query, "results.json", format="json", decompress=True) 
   result_set = download(query, "results.json.gz", format="json", decompress=False) 

Retrieving these result sets can take a long time, depending on their size.
Both the ``download`` and ``search`` functions accept a ``progress_callback`` function
as a keyword argument. This function is called when the query is submitted to the server
and frequently as the query is processed and a response prepared. This callback is always
called with a ``search.JobStatus`` as its first argument; it should also handle arbitrary
keyword arguments, even if you don't do anything with them. A simple example to print the
status of the job and to demonstrate what keyword arguments are passed to the callback
at different stages of the job's life:

.. code:: python
   
   def progress_callback(status, **kwargs):
       print("Search status: {}; Received kwargs: {}".format(status, kwargs))

   result_set = search(query, progress_callback=progress_callback) 
