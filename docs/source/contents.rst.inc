User's Guide
------------

.. toctree::
   :maxdepth: 2

   installation
   tutorial
   cli
   troubleshooting

API Reference
-------------

.. toctree::
   :maxdepth: 2

   api

Additional Notes
----------------

.. toctree::
   :maxdepth: 2

   acknowledgements
   upgrading
   changelog
   license

