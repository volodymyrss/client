The ANTARES Client
==================

Thank you for visiting the documentation for the ANTARES client. Begin with
:ref:`installation`. If you'd like to work with data from ANTARES without
writing any code yourself, see :ref:`cli`. If you'd like to customize the
behavior of the client or incorporate its functionality in your own Pyton code,
see the :ref:`tutorial` for a quick introduction and the :ref:`api` for a
complete reference.

.. include:: contents.rst.inc
